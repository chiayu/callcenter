package chiayu.demo.callcenter.controller;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import chiayu.demo.callcenter.model.Employee;
import chiayu.demo.rabbitmq.QueueService;

@Controller
@EnableAutoConfiguration

@RequestMapping("/build")
public class BuildQueueController {

	private static Logger mLogger = Logger.getLogger(BuildQueueController.class);

	@Autowired
	private QueueService mQueueService;

	@RequestMapping("/test")
	@ResponseBody
	String heartbeating() {
		return "{\"heartbeating\":\"test\"}";
	}

	@RequestMapping("/initial/all")
	@ResponseBody
	String initialAllQueues() throws IOException, TimeoutException {

		for (Employee em : Employee.values() ) {
			mQueueService.buildQueue(em.name(), em.getQuantity()); // TODO:
			mLogger.info(em.name()+"'s  queue is built and size is " + em.getQuantity());			
		}
		return "{\"result\":\"test\"}";
	}

	@RequestMapping("/initial")
	@ResponseBody
	String initialQueue(@RequestParam(value = "name", required = true) String pName,
			@RequestParam(value = "size", required = true) int pSize) throws IOException, TimeoutException {

		mQueueService.buildQueue(pName, pSize); // TODO:
		mLogger.info("building....queue...." + pName + ":" + pSize);

		return "{\"result\":\"test\"}";
	}
}