package chiayu.demo.callcenter.controller;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import chiayu.demo.callcenter.model.Employee;
import chiayu.demo.rabbitmq.QueueService;

@Controller
@EnableAutoConfiguration
@RequestMapping("/consumer")
public class ConsumerController {

	private static Logger mLogger = Logger.getLogger(ConsumerController.class);

	@Autowired
	private QueueService mQueueService;

	@RequestMapping("/binding/all")
	@ResponseBody
	String binding(HttpServletRequest request) throws IOException, TimeoutException {
		mQueueService.bindingConsumer(Employee.FRESHMAN.name());
		mQueueService.bindingConsumer(Employee.PM.name());
		mQueueService.bindingConsumer(Employee.TL.name());
		return "{result:done}";
	}
}
