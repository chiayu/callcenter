package chiayu.demo.callcenter.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import chiayu.demo.callcenter.model.Employee;
import chiayu.demo.rabbitmq.QueueService;
import chiayu.demo.rabbitmq.exception.ExcessQueueSizeException;

@Controller
@EnableAutoConfiguration

@RequestMapping("/sender")
public class SenderController {


	private static Logger mLogger = Logger.getLogger(SenderController.class);

	@Autowired
	private QueueService mQueueService;

	@RequestMapping("/test")
	@ResponseBody
	String heartbeating() {
		return "{\"heartbeating\":\"test\"}";
	}

	
	@RequestMapping("/freshman")
	@ResponseBody
	Map<String,String> open(HttpServletRequest request) throws IOException, TimeoutException, InterruptedException {
		Map<String,String> result = new HashMap<String,String>();
		try {
			mQueueService.sendToQueue(Employee.FRESHMAN.name(), "fressssssss");
		} catch (ExcessQueueSizeException e) {
			mLogger.error(Employee.FRESHMAN.name() + "over size");
			result.put("result", "false");
			return result;
		}
		
		result.put("result", "true");
		return result;
	}
	@RequestMapping("/freshman2")
	@ResponseBody
	String open2(HttpServletRequest request) throws IOException, TimeoutException, InterruptedException {
		try {
			mQueueService.sendToQueue(Employee.FRESHMAN.name(), "noooooonono");
		} catch (ExcessQueueSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	
	@RequestMapping("/tl")
	@ResponseBody
	Map<String,String> send2tl(HttpServletRequest request) throws IOException, TimeoutException, InterruptedException {
		Map<String,String> result = new HashMap<String,String>();
		try {
			mQueueService.sendToQueue(Employee.TL.name(), "tl");
		} catch (ExcessQueueSizeException e) {
			mLogger.error(Employee.TL.name() + "over size");
			result.put("result", "false");
			return result;
		}
		
		result.put("result", "true");
		return result;
	}
	
	@RequestMapping("/pm")
	@ResponseBody
	Map<String,String> send2pm(HttpServletRequest request) throws IOException, TimeoutException, InterruptedException {
		Map<String,String> result = new HashMap<String,String>();
		try {
			mQueueService.sendToQueue(Employee.PM.name(), "pm");
		} catch (ExcessQueueSizeException e) {
			mLogger.error(Employee.PM.name() + "over size");
			result.put("result", "false");
			return result;
		}
		
		result.put("result", "true");
		return result;
	}

	
}