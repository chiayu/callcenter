package chiayu.demo.callcenter.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ability")
public class AbilityController {

	private static Logger mLogger = Logger.getLogger(AbilityController.class);

	@RequestMapping("/heartbeating")
	@ResponseBody
	String heartbeating() {
		return "{\"heartbeating\":\"test\"}";
	}

	@RequestMapping("/solve")
	@ResponseBody
	Map<String, String> solvable() {
		Map<String, String> result = new HashMap<String, String>();
		Random rand = new Random();
		result.put("result", String.valueOf(rand.nextBoolean()));
		return result;
	}

}
