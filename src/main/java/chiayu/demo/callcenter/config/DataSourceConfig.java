package chiayu.demo.callcenter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.rabbitmq.client.ConnectionFactory;

@Configuration
public class DataSourceConfig {

	private static ConnectionFactory rabbitMQfactory = null;// new ConnectionFactory();

	public void setRabbitMQConnectionFactory() {
		rabbitMQfactory.setPassword("guest");
		rabbitMQfactory.setUsername("guest");
//		rabbitMQfactory.setVirtualHost("test");
		rabbitMQfactory.setHost("my.rabbitmq.tw");
//		rabbitMQfactory.DEFAULT_USER
		rabbitMQfactory.setPort(5672);
	}

	@Bean(name = "rabbitMQFactory")
	public ConnectionFactory getRabbitMQConnectionFactory() {

		if (rabbitMQfactory == null) {
			rabbitMQfactory =  new ConnectionFactory();
			setRabbitMQConnectionFactory();
		}
		return rabbitMQfactory;
	}

}
