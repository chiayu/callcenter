package chiayu.demo.callcenter.model;

public enum Employee {

	FRESHMAN("freshman",10),
	TL("technical lead",1),
	PM("product manager",1);
	
	private String title;
	private int quantity;
	
	private Employee(String title, int quantity){
		this.title = title;
		this.quantity = quantity;		
	}

	public String getTitle() {
		return this.title;
	}
	
	public int getQuantity() {
		return this.quantity;
	}
}
