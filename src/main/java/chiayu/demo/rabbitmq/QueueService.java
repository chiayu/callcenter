package chiayu.demo.rabbitmq;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Service;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.GetResponse;
import com.rabbitmq.client.MessageProperties;

import chiayu.demo.callcenter.model.Employee;
import chiayu.demo.rabbitmq.exception.ExcessQueueSizeException;

@EnableAutoConfiguration
@Service
public class QueueService extends RabbitMQTemplate {

	private String queueName;
	private String message;
	private static Logger mLogger = Logger.getLogger(QueueService.class);

	public void sendToQueue(String queueName, String message) throws IOException, TimeoutException, ExcessQueueSizeException {
		this.queueName = queueName;
		this.message = message;
		super.executeSend();
	}

	public void bindingConsumer(String queueName) throws IOException, TimeoutException {
		this.queueName = queueName;
		super.executeBindingConsumer();
	}
	
	public void buildQueue(String queueName, int queueSize) throws IOException, TimeoutException {
		super.setSessiongFactory(sessiongFactory);
		Connection connection = sessiongFactory.newConnection();
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("x-max-length", queueSize);
		args.put("x-overflow", "reject-publish");
		
		Channel channel = connection.createChannel();

		channel.queueDelete(queueName);
		channel.queueDeclare(queueName, false, false, false, args);
//		for (int i = 0; i < queueSize; i++) {
//			String message = UUID.randomUUID().toString();
//			channel.basicPublish("", queueName, null, message.getBytes());
//			System.out.println(" [x] build queue '" + message + "'");
//		}
		channel.close();
		connection.close();
	}

	public String getFormQueue(String queueName, String message) throws IOException, TimeoutException {
		this.queueName = queueName;
		this.message = message;
		return super.executeConsumer();
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public void handleSender(Channel channel) throws IOException, ExcessQueueSizeException {
		super.setSessiongFactory(sessiongFactory);

		int queueSize = Employee.valueOf(queueName).getQuantity();
		if(channel.messageCount(queueName) >= queueSize ) {
			throw new ExcessQueueSizeException("over.."+queueSize);
		}
		channel.basicPublish("", queueName, MessageProperties.PERSISTENT_BASIC, message.getBytes());

		channel.confirmSelect();
		mLogger.info(" [x] Sent '" + message + "'");
	}

	@Override
	public String handleConsumer(Channel channel) throws IOException {
		super.setSessiongFactory(sessiongFactory);
		Map<String, String> result = new HashMap<String, String>();

		channel.queueDeclare(queueName, false, false, false, null);
		// channel.basicQos(1);
		boolean autoAck = false;
		GetResponse response = channel.basicGet(queueName, autoAck);
		if (response == null) {
			// No message retrieved.
			System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		} else {
			AMQP.BasicProperties props = response.getProps();
			byte[] body = response.getBody();
			result.put("message", new String(body));
			result.put("result", "true");
			System.out.println(" [x] Received '" + new String(body) + "'");
		}

		return result.get("message");
	}

	

	@Override
	public void bindingConsumer(Channel channel) throws IOException, TimeoutException {
	
		final Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				 mLogger.info(" [x] Received '" + message + "'");
				try {
					Thread.sleep(2000);
//					doWork(message);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
				    mLogger.info(" [x] Done");
				}
			}
		};
		channel.basicQos(1);
		channel.basicConsume(queueName, true, consumer);

	}
}
