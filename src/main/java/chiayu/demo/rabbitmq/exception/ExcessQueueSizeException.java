package chiayu.demo.rabbitmq.exception;

public class ExcessQueueSizeException extends Exception {

	public ExcessQueueSizeException() {
		super();
	}

	public ExcessQueueSizeException(String message) {
		super(message);
	}

	public ExcessQueueSizeException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExcessQueueSizeException(Throwable cause) {
		super(cause);
	}
}
