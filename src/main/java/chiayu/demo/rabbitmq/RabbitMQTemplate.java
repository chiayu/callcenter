package chiayu.demo.rabbitmq;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import chiayu.demo.rabbitmq.exception.ExcessQueueSizeException;

@EnableAutoConfiguration
@Component
public abstract class RabbitMQTemplate {

	@Autowired
	@Qualifier("rabbitMQFactory")
	public ConnectionFactory sessiongFactory;

	
	// template method, final so subclasses can't override

	public final void executeSend() throws IOException, TimeoutException, ExcessQueueSizeException {

		Connection connection = sessiongFactory.newConnection();

		Channel channel = connection.createChannel();

		handleSender(channel);
		// channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		// String message = "" +pId;
		// mLogger.info(" [x] Sent '" + message + "'");
		// channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
		// System.out.println(" [x] Sent '" + message + "'");
		channel.close();
		connection.close();
	}

	public void setSessiongFactory(ConnectionFactory sessiongFactory) {
		this.sessiongFactory = sessiongFactory;
	}
	
	public final String executeConsumer() throws IOException, TimeoutException {

		Connection connection = sessiongFactory.newConnection();
		Channel channel = connection.createChannel();
		String result = handleConsumer(channel);
		channel.close();
		connection.close();
		return result;
	}
	
	public final void executeBindingConsumer() throws IOException, TimeoutException {

		Connection connection = sessiongFactory.newConnection();
		Channel channel = connection.createChannel();
		bindingConsumer(channel);
//		channel.close();
//		connection.close();
//		return result;
	}

	public abstract String handleConsumer(Channel channel) throws IOException;

	public abstract void handleSender(Channel channel) throws IOException, ExcessQueueSizeException;

	public abstract void bindingConsumer(Channel channel) throws IOException, TimeoutException;

}
