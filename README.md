# CallCenter

Imagine you have a call center with three levels of employees: fresher, technical lead (TL), product manager (PM). There can be multiple employees, but only one TL or PM. An incoming telephone call must be allocated to a fresher who is free. If no freshers are free, or if the current fresher is unable to solve the caller's problem (determined by a simple dice roll), he or she must escalate the call to technical lead. If the TL is not free or not able to handle it, then the call should be escalated to the PM.

## Required
docker

### Workflow
![enter image description here](https://bytebucket.org/chiayu/callcenter/raw/0007e73d6b46fafccbc026569e74e1913344d0ff/workflow.png)
### Architect
![enter image description here](https://bytebucket.org/chiayu/callcenter/raw/0007e73d6b46fafccbc026569e74e1913344d0ff/callcenter.png)

* apache [dockerhub](https://hub.docker.com/r/cyh345/apache2/)	
	* For the views, and control the workflow.
* springboot [bitbucket](https://bitbucket.org/chiayu/callcenter)	 [dockerhub](https://hub.docker.com/r/cyh345/callcenter-api/)
	* For the apis, which connect to queues.
* rabbitMQ [dockerhub](https://hub.docker.com/_/rabbitmq/)
	* For queuing.
	
## Pull images

docker pull cyh345/apache2
docker pull cyh345/callcenter-api
docker pull rabbitmq

## Configure [docker-compose.yml](https://bitbucket.org/chiayu/callcenter/src/4237899b5062c91da39ed49824cdbbc9a8647548/docker-compose.yml?at=master&fileviewer=file-view-default)

* in line 17, 25

	extra_hosts:
	    "my.rabbitmq.tw:**10.1.1.17**"

* 10.1.1.17 - > should be your local docker eth

## Configure hosts in local

*	Add hosts to  /etc/hosts
	127.0.0.1 my.rabbitmq.tw

## Running
* Download [docker-compose.yml](https://bitbucket.org/chiayu/callcenter/src/4237899b5062c91da39ed49824cdbbc9a8647548/docker-compose.yml?at=master&fileviewer=file-view-default)
* Run command in the same directory as downloaded file
	
		docker-compose up
	
    
*	open "my.rabbitmq.tw" in browser chrome

